"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    
    # Calculate the squared magnitude at each time point
    squared_magnitude = x**2 + y**2 + z**2

    # Take the square root to get the absolute value
    vec_accel = np.sqrt(squared_magnitude)

    return vec_accel

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    # Create linearly spaced interpolation points over the entire time range
    interpolation_points = np.linspace(min(time), max(time), num=len(time))

    # Calculate slopes (delta y / delta x)
    slopes = np.diff(data) / np.diff(time)

    # Perform linear interpolation
    interpolated_values = np.interp(interpolation_points, time, data, left=data[0], right=data[-1])

    return interpolation_points, interpolated_values


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    # Calculate the FFT
    fft_result = np.fft.fft(x)
    
    # Calculate the corresponding frequencies
    frequencies = np.fft.fftfreq(len(time), time[1] - time[0])
    
    # Take the absolute value to get the amplitude
    amplitude = np.abs(fft_result)
    
    return amplitude, frequencies