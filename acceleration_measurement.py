import time
import h5py
import adafruit_adxl34x
import numpy as np
import board
import json
import os

from functions.m_operate import prepare_metadata
from functions.m_operate import log_JSON
from functions.m_operate import set_sensor_setting

"""Parameter definition"""
# -------------------------------------------------------------------------------------------#1-start
# TODO: Adjust the parameters to your needs
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
path_setup_json = "datasheets/setup_speaker.json"  # adjust this to the setup json path
measure_duration_in_s = 20

# ---------------------------------------------------------------------------------------------#1-end

"""Prepare Metadata and create H5-File"""
(
    setup_json_dict,
    sensor_settings_dict,
    path_h5_file,
    path_measurement_folder,
) = prepare_metadata(path_setup_json, path_folder_metadata="datasheets")

print("Setup dictionary:")
print(json.dumps(setup_json_dict, indent=2, default=str))
print()
print("Sensor settings dictionary")
print(json.dumps(sensor_settings_dict, indent=2, default=str))
print()
print(f"Path to the measurement data h5 file created: {path_h5_file}")
print(f"Path to the folder in which the measurement is saved: {path_measurement_folder}")


"""Establishing a connection to the acceleration sensor"""
i2c = board.I2C()  # use default SCL and SDA channels of the pi
try:
    accelerometer = adafruit_adxl34x.ADXL345(i2c)
except Exception as error:
    print(
        "Unfortunately, the ADXL345 accelerometer could not be initialized.\n \
           Make sure your sensor is wired correctly by entering the following\n \
           to your pi's terminal: 'i2cdetect -y 1' "
    )
    print(error)


# Initialize the data structure
sensor_id = sensor_settings_dict["ID"]
res = {sensor_id : {"acceleration_x" : [], "acceleration_y" : [], "acceleration_z" : [], "timestamp" : []}}
print(res)
# Measure the probe
measure_time = time.time()
while (time.time() - measure_time) < measure_duration_in_s:
    res[sensor_id]["acceleration_x"].append(accelerometer.acceleration[0]) 
    res[sensor_id]["acceleration_y"].append(accelerometer.acceleration[1])
    res[sensor_id]["acceleration_z"].append(accelerometer.acceleration[2])
    res[sensor_id]["timestamp"].append(time.time())
    time.sleep(0.001)
# Write results in hdf5
with h5py.File(path_h5_file, "w") as f:
    rawdata_grp = f.create_group("RawData")
    sensor_grp = rawdata_grp.create_group(sensor_id)
    
    sensor_grp.create_dataset("timestamp", data = res[sensor_id]["timestamp"])
    sensor_grp.create_dataset("acceleration_x", data = res[sensor_id]["acceleration_x"])
    sensor_grp.create_dataset("acceleration_y", data = res[sensor_id]["acceleration_y"])
    sensor_grp.create_dataset("acceleration_z", data = res[sensor_id]["acceleration_z"])

"""Log JSON metadata"""
log_JSON(setup_json_dict, path_setup_json, path_measurement_folder)
print("Measurement data was saved in {}/".format(path_measurement_folder))
